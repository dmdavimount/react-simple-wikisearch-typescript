import React, { useState } from "react";

import "./Card.scss";

type CardProps = {
  title?: string;
  body?: string;
  link?: string;
  image?: string;
  expandable?: boolean;
};

const Card = ({ title, body, link, image, expandable = false }: CardProps) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <>
      {title && (
        <div className="card-comp">
          <div className="card-header">
            {image && (
              <span className="card-thumb">
                <img src={image} alt="thumb" />
              </span>
            )}
            <span className="card-title">
              {link ? (
                <a href={link} target="_Blank" rel="noopener noreferrer">
                  {title}
                </a>
              ) : (
                title
              )}
            </span>
          </div>
          {body && (
            <p className={`card-body ${expanded ? "expanded" : "shortened"}`}>
              {body}
            </p>
          )}
          {expandable && (
            <div
              className={`card-expand ${expanded ? "opened" : "closed"}`}
              onClick={() => setExpanded(!expanded)}
            >
              Show {expanded ? "less" : "more"}
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default Card;
