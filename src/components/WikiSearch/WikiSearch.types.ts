export type WikiAPIThumbType = {
  source: string;
};

export type WikiAPIType = {
  title: string;
  extract: string;
  pageid: string;
  thumbnail: WikiAPIThumbType;
};

export type WikiSearchProps = {
  lang: string;
};

export type WikiSearchState = {
  query: string | null;
  search: Array<WikiAPIType>;
  loading: boolean;
  error: boolean;
};
