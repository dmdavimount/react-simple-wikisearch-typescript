import React from "react";

import { ReactComponent as SearchIcon } from "./search_icon.svg";

import "./Loader.scss";

export default ({ error = false }) => (
  <div className="loader-comp">
    <span>
      <SearchIcon />
    </span>
  </div>
);
